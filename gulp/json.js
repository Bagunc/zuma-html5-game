import { src, dest, series } from 'gulp'

export const execute = browser => next => {

  try {
    src([
      'src/assets/json/**/*'
    ])
    .pipe(dest(
      'public/assets/json'
    ))
    .pipe(browser.stream())
  } catch (err) {
    console.log(err)
  }
  next()
}

export default (browser) => series(
  execute(browser)
)