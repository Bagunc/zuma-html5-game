/**
 * @name gulp-start
 * @description Empty project based on gulp
 * 
 * @version 1.1.0
 * 
 * @package SkyTech studio
 * @author Tatul Harutyunyan
 */

import { watch, series, src,  } from 'gulp'

import clean from 'gulp-clean'
import browserSync from 'browser-sync'
import { existsSync, mkdirSync } from 'fs'

import taskPug from './gulp/pug'
import taskJson from './gulp/json'
import taskFonts from './gulp/fonts'
import taskImages from './gulp/images'
import taskSounds from './gulp/sounds'
import taskStyles from './gulp/styles'
import taskScripts from './gulp/scripts'
import taskVendors from './gulp/vendors'

const browser = browserSync.create()

export const pug = taskPug(browser)
export const json = taskJson(browser)
export const fonts = taskFonts(browser)
export const images = taskImages(browser)
export const sounds = taskSounds(browser)
export const styles = taskStyles(browser)
export const scripts = taskScripts(browser)
export const vendors = taskVendors(browser)

export const cleaning = () => {
  
  return src('public/**', { read: true })
    .pipe(clean({ force: true }))
}

export const building = next => {

  if (!existsSync('public'))
    mkdirSync('public')

  next()
}

export const watching = next => {

  watch('gulp/**/*', build)

  watch('src/**/*.pug', pug)
  watch('src/**/*.sass', styles)
  watch('src/assets/json/**/*', json)
  watch('src/assets/js/**/*', scripts)
  watch('src/assets/img/**/*', images)
  watch('src/assets/sounds/**/*', sounds)
  watch('src/assets/vendors/**/*', vendors)
  watch('src/assets/fonts/**/*', series(fonts, styles))
  
  watch('gulp/**/*').on('change', browser.reload)
  watch('gulp.utils.js').on('change', browser.reload)
  watch('gulpfile.babel.js').on('change', browser.reload)
  
  next()
}

export const start = next => {
  browser.init({
    port: 3000,
    server: 'public/',
  })

  next()
}

export const build = series(
  building,

  pug,
  json,
  fonts,
  images,
  sounds,
  styles,
  vendors,
  scripts,
  
  next => next()
)

export const serve = series(
  build,
  
  start,
  watching
)

export default serve