const soundID = "combo"

const init = () => {
  const stage = new createjs.Stage('canvas')

  var circle = new createjs.Shape()
  circle.graphics.beginFill("DeepSkyBlue").drawCircle(0, 0, 50)
  circle.x = 100
  circle.y = 100
  stage.addChild(circle)
  circle.addEventListener('click', () => createjs.Sound.play(soundID))


  const spriteSheet = new createjs.SpriteSheet({
    images: ['/assets/img/sprites/ball_0.png'],
    frames: {
        width: 30,
        height: 30,
    },
    animations: {
        move: [0, 49]
    }
  });
  var ball = new createjs.Sprite(spriteSheet, "run");
  ball.x = 0
  ball.y = 0
  stage.addChild(ball)

  createjs.Ticker.on("tick", event => {
    stage.update(event)
  });

}

const loadSounds = () => {
  createjs.Sound.registerSound('/assets/sounds/combo.mp3', soundID)

}

const loadImages = () => {

  const preloader = new createjs.LoadQueue()
  preloader.addEventListener('fileload', event => {
    
  })
  preloader.loadFile('/assets/img/sprites/200x200.jpg')

}

window.onload = () => {
  
  init()
  loadSounds()
  loadImages()
}