import config from './config'

export default () => {

  const configs = config().then(response => response.json())
                            .then(data => console.log(data))


  const setLocalStorageLevel = (level) => {
    config.maxLevel < level && localStorage.setItem('sorcerer_level', level)
  }


  const clearLocalStorage = () => localStorage.clear()

}